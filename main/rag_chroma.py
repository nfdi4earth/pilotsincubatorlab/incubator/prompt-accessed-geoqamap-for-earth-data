import chromadb,time,os
# Example setup of the client to connect to your chroma server
from dotenv import load_dotenv
load_dotenv()
from rag_model_openai import get_embedding


os.environ['OPENAI_API_KEY']=os.getenv("OPENAI_API_KEY")

client = chromadb.HttpClient(host="my_chroma", port=8000)


collection = client.get_or_create_collection("buildings_name_vec")
import chromadb.utils.embedding_functions as embedding_functions
openai_ef = embedding_functions.OpenAIEmbeddingFunction(
                api_key=os.environ['OPENAI_API_KEY'],
                model_name="text-embedding-3-small"
            )
def calculate_similarity_chroma(text_str,results_num=60):
    results = collection.query(
        query_embeddings=list(get_embedding(text_str)),
        n_results=results_num,

        # where={"metadata_field": "is_equal_to_this"}, # optional filter
        # where_document={"$contains":text_str}  # optional filter
    )

    # print((results['distances'][0]))
    # print((results['documents'][0]))
    distances = results['distances'][0]
    documents = results['documents'][0]

    # 过滤结果
    filtered_results = [documents[i] for i in range(len(distances)) if distances[i] < 0.5]
    close_results = [documents[i] for i in range(len(distances)) if distances[i] < 0.21]
    if close_results:
        return close_results
    else:
        return filtered_results
