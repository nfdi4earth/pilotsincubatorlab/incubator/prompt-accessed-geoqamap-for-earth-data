import pkg_resources

# List of packages to check
packages = [
    "shapely", "psycopg2", "rdflib", "tqdm", "SPARQLWrapper",
    "geopandas", "geopy", "openai", "chromadb", "python-dotenv",
    "flask", "pandas", "spacy",'user_agents','flask_socketio','sentence_transformers','torch'
]

# Function to get the installed version of each package
def get_package_versions(packages):
    package_versions = {}
    for package in packages:
        try:
            version = pkg_resources.get_distribution(package).version
            package_versions[package] = version
        except pkg_resources.DistributionNotFound:
            package_versions[package] = "Not installed"
    return package_versions

# Get package versions
package_versions = get_package_versions(packages)

# Format the output for a requirements.txt file
requirements = "\n".join([f"{pkg}=={ver}" for pkg, ver in package_versions.items() if ver != "Not installed"])

print(requirements)
