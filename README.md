# Prompt accessed GeoQAMap for Earth Data

<img src="logo.png" alt="GeoQAMap_logo" width="45"/>

### Abstract
The project presents a multi-agent-based spatial data analysis framework that allows intuitive interaction with vector geospatial data through natural language queries. The framework is designed to support cross-querying of geospatial data stored in various formats, including relational and graph databases. By providing a simple description of data formats, it ensures compatibility between different types of databases. Advanced tools integrated into the framework facilitate vector-store-based fuzzy searches, offering more nuanced and flexible data retrieval. Comprehensive visualization tools provide a user-friendly platform, enabling step by step spatial analysis, making this framework suitable for exploratory spatial data anal-ysis. This approach addresses the limitations of traditional query languages like SQL or SPARQL, which typically require high expertise. The results of this project demonstrate the potential for more accessible data querying and analysis, enhancing user interaction with geo-spatial data from Earth System Science.

<img src="example.PNG" alt="GeoQAMap_example" width="500"/>

## Background
The project aims to simplify interactions with geospatial data. Traditional querying requires complex languages such as SQL or SPARQL, posing a challenge to non-expert users. More-over, Geographic Information System (GIS) software often requires specialized expertise to be used proficiently. This research proposes a natural language-based multi-agent spatial data analysis framework to make these interactions more accessible and intuitive.

## Results

In this project, we developed GeoQAMap, an innovative Q&A system designed to enable users to interact with vector map data seamlessly. It was specifically designed for users across various skill levels, from GIS and SQL experts to those less familiar with geospatial technologies. Key Features and Capabilities:

- Spatial Analysis through Natural Language: GeoQAMap supports a variety of fundamental spatial analysis operations that users can perform via natural language input.

- Multi-criteria Geographic Entity Query: GeoQAMap not only supports simple, explicit queries but also handles complex or implicit queries involving multiple data fields of one data or across multiple data sources. 

- Support for Follow-up Questions: GeoQAMap features robust support for continuous Q&A, allowing users to follow up on initial queries with additional, related questions. 

- Exceptional User Experience: GeoQAMap has demonstrated outstanding performance in 4 key metrics out of 6, including attractiveness, efficiency, stimulation, and novelty.


## Outcomes and Trends
- GitHub code repository with documentations and docker files for easy deployment.
- Web interface for visualization of the query results.


## How to deploy
Keep the ports 8000, 9090, 5432 free.


1.Clone the repo：
```
git clone https://github.com/nitpicker55555/Geo-QA.git
cd Geo-QA
```
2.Add OPENAI_API_KEY in file '.env', like：
```
OPENAI_API_KEY=sk-xxx
```

3.Clean up any existing containers or images as needed:
```
docker system prune --volumes
docker image prune -a
```

4.Build docker image:
```
docker-compose -f docker-compose.yml up
```
It should take 5 minutes to set up.  


If it is finished, you will see:

```
geo_qa  |  * Running on all addresses (0.0.0.0)
geo_qa  |  * Running on http://127.0.0.1:9090
geo_qa  |  * Running on http://xxx.xx.x.x:9090
geo_qa  | Press CTRL+C to quit
```

Open [localhost:9090](http://localhost:9090/) to see query interface:

*If you do not need tutorial, you can close it by clicking the red **Stop Tips** button.*

<img src="interface.png" alt="workflow" width="300"/>

*For an introduction to the database and the supported spatial relationships, please click the **Introduction** button.*

## Resources
•	[Video](https://www.youtube.com/watch?v=GeWgN2fMFkE)

## Publication
•	Feng, Y., Zhang, P., Ding, L., Xiao, G., & Meng, L. (2024) GeoQAS: a Multi-agent LLM Powered Geographic Question Answering System for Data Retrieval and Spatial Analysis (in preparation).

## Funding
This work has been funded by the German Research Foundation (NFDI4Earth,
DFG project no. 460036893, https://www.nfdi4earth.de/).
