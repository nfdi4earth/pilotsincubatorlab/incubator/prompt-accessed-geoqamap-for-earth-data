
FROM python:3.9-slim


WORKDIR /app


COPY ./.env /app/
COPY ./main /app
RUN apt-get update && apt-get install -y unzip
RUN pip install gdown
RUN gdown --id 1ejPBCtjd0K9ppW2cw1Gv1cESWCz44YEm -O downloaded_file.zip


RUN unzip downloaded_file.zip -d /app/


RUN mkdir -p /docker-entrypoint-initdb.d
RUN cp /app/data/init_postgis.sql /docker-entrypoint-initdb.d/00_init_postgis.sql
RUN cp -r /app/data/* /docker-entrypoint-initdb.d/


RUN pip install --no-cache-dir -r requirements.txt
RUN python -m spacy download en_core_web_sm


CMD ["python", "app_changed_agent.py"]
